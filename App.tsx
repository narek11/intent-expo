import React from 'react';
import {StatusBar} from 'expo-status-bar';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import Navigation from './src/navigation';


const App = () => (
    <SafeAreaProvider>
        <Navigation />
        <StatusBar />
    </SafeAreaProvider>
)
export default App;
