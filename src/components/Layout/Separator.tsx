import React from 'react';
import {View} from 'react-native';

interface SeparatorProps {
    height?: number;
}

export const Separator = ({height = 10}: SeparatorProps): JSX.Element => (
    <View style={{height}} />
);
