import * as React from 'react'
import {FlatList, Text, TouchableOpacity, View} from 'react-native'
import {Separator} from './Layout'
import {UniversityType} from '../models/UniversityModel';
import 'react-native-get-random-values'
import {nanoid} from "nanoid";

interface UniversityListProps {
    list: UniversityType[];
    onItemPress: (url: string) => void
}

interface UniversityProps {
    name: string;
    onItemPress: (url: string) => void
}


export const University = React.memo(({name, onItemPress}: UniversityProps): JSX.Element => (
        <TouchableOpacity style={styles.universityContainer} onPress={onItemPress}>
            <Text style={styles.customer}>{name}</Text>
        </TouchableOpacity>
    )
)

export const UniversityList = (props: UniversityListProps): JSX.Element => (
    <FlatList
        data={props.list}
        keyExtractor={() => nanoid()}
        contentContainerStyle={styles.container}
        renderItem={({item: {name, website}, index}) => (
            <University
                key={index}
                name={name}
                onItemPress={() => props.onItemPress(website)}
            />
        )
        }
        ItemSeparatorComponent={() => <Separator />}
        showsVerticalScrollIndicator={false}
        ListEmptyComponent={() => (
            <View>
                <Text>No universities found</Text>
            </View>
        )}
    />
)

const styles = {
    container: {
        marginHorizontal: 10
    },
    universityContainer: {
        borderWidth: 1,
        borderColor: 'grey',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    customer: {
        fontSize: 17,
        padding: 10
    }
}

export default UniversityList
