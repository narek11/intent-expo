export interface UniversityType {
    alpha_two_code: string;
    country: string;
    domains: string[];
    name: string;
    'state-province'?: any;
    web_pages: string[];
    website: string;
}
