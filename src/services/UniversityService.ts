import {URLS} from '../constants/api';
import {errorHandler, responseHandler} from '../utils';
import {UniversityType} from '../models/UniversityModel';
import {errorType} from '../utils'

interface GetUniversitiesType {
    country: string;
}


export const getUniversities = async ({country}: GetUniversitiesType): Promise<UniversityType[] | errorType> => {
    try {
        const result = await fetch(`${URLS.SEARCH_UNIVERSITIES}?country=${encodeURIComponent(country)}`)

        return responseHandler(result)
    } catch (e) {
        return errorHandler(e)
    }
};
