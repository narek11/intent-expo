import React from 'react'
import {ActivityIndicator, Linking} from 'react-native'
import {UniversityType} from '../models/UniversityModel';
import {UniversityService} from '../services';
import UniversityList from '../components/UniversityList';
import {prepareUniversities} from '../utils';

interface CountriesScreenProps {
    country: string;
}

const CountriesScreen = (props: CountriesScreenProps): JSX.Element => {
    const [loading, setLoading] = React.useState<boolean>(true)
    const [universities, setUniversities] = React.useState<UniversityType[]>([])

    React.useEffect(() => {
        (async () => {
            const list = await UniversityService.getUniversities({country: props.route.params.country})

            if (!list.error) {
                setUniversities(list)
            }

            setLoading(false)

        })()
    }, [])


    if (loading) {
        return (
            <ActivityIndicator />
        )
    }


    return (
        <UniversityList list={prepareUniversities(universities)} onItemPress={(url) => {
            url && Linking.openURL(url)
        }} />
    )

}

export default CountriesScreen
