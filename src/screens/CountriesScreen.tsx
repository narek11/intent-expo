import React from 'react'
import {ScrollView, Text, TouchableOpacity} from 'react-native'
import {useNavigation} from "@react-navigation/native";
import {Routes} from "../navigation/routes";
import colors from "../constants/colors";

// // Poland, Germany, France, Spain, and the United Kingdom
const countries = [
    {name: 'Poland', queryParam: 'Poland'},
    {name: 'Germany', queryParam: 'Germany'},
    {name: 'France', queryParam: 'France'},
    {name: 'Spain', queryParam: 'Spain'},
    {name: 'United Kingdom', queryParam: 'United Kingdom'},
]

interface CountryInterface {
    name: string
}

const Country = ({name}: CountryInterface) => {
    const navigation = useNavigation()
    return (
        <TouchableOpacity style={countryStyles.container} onPress={() => navigation.navigate(Routes.Universities, {country: name})}>
            <Text style={countryStyles.text}>{name}</Text>
        </TouchableOpacity>
    )
}
const countryStyles = {
    container: {
        marginTop: 3,
        padding: 10,
        backgroundColor: colors.pink
    },
    text: {
        fontSize: 17,
        color: colors.grey
    }
}

const CountryList = () => {
    return (
        <ScrollView contentContainerStyle={{flex: 1, height: 200}}>
            {countries.map((ct) => (
                <Country name={ct.name} key={ct.name} />
            ))}
        </ScrollView>
    )
}

const CountriesScreen = (): JSX.Element => {
    return (
        <CountryList />
    )

}

export default CountriesScreen
