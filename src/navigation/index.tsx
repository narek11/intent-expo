import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import CountriesScreen from '../screens/CountriesScreen';
import UniversitiesScreen from '../screens/UniversitiesScreen';
import {Routes} from './routes'

const Stack = createNativeStackNavigator();
const Navigation = (): JSX.Element => {
    return (
        <NavigationContainer>
            <Stack.Navigator>
                <Stack.Screen name={Routes.Countries} component={CountriesScreen}  />
                <Stack.Screen name={Routes.Universities} component={UniversitiesScreen}
                              options={({route}) => ({title: route.params.country})} />
            </Stack.Navigator>
        </NavigationContainer>
    );
}

export default Navigation;
