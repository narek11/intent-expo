const colors = {
    white: '#FFFFFF',
    black: '#000000',
    pink: 'pink',
    purple: 'purple',
    grey: 'grey'
}

export default colors;
