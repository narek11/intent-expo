import {UniversityType} from "../models/UniversityModel";

export const prepareUniversities = (universities: UniversityType[]) => {
    const list = universities.map((un: UniversityType) => {
        let website = ''
        const {web_pages} = un;
        if (Array.isArray(web_pages) && web_pages.length > 0) {
            website = web_pages[0]
        }

        return {...un, website}
    })//map

    return list
}
